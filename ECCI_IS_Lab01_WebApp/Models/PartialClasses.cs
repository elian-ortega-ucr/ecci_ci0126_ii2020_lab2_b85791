﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECCI_IS_Lab01_WebApp.Models.Metadata
{
    public class PartialClasses
    {
        //Connection EstudianteMetadata with Estudiante
        [MetadataType(typeof(EstudianteMetadata))]
        public partial class Estudiante { }

        //Connection MatriculaMetadata with Matricula
        [MetadataType(typeof(MatriculaMetadata))]
        public partial class Matricula { }
    }
}